import { connect } from 'react-redux';
import CounterGrid from '../components/CounterGrid';
import {
  incrementCounter,
  decrementCounter,
  addCounter,
  removeCounter
} from '../actions';

function mapStateToProps(state) {
  return { counters: state.counters };
}

function mapDispatchToProps(dispatch) {
  return {
    onIncrement: (index) => dispatch(incrementCounter(index)),
    onDecrement: (index) => dispatch(decrementCounter(index)),
    addCounter: () => dispatch(addCounter()),
    removeCounter: () => dispatch(removeCounter())
  }
}

const CounterList = connect(
  mapStateToProps,
  mapDispatchToProps
)(CounterGrid)

export default CounterList;