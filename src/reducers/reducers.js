'use strict';

import {
  INCREMENT_COUNTER,
  DECREMENT_COUNTER,
  ADD_COUNTER,
  REMOVE_COUNTER
} from '../actions';

function counters(state={counters: [0,0,0]}, action) {
  switch (action.type) {
    case INCREMENT_COUNTER:
      console.log('incrementing the counter' + action.index);
      return Object.assign({}, {
        counters: [
          ...state.counters.slice(0, action.index),
          state.counters[action.index] + 1,
          ...state.counters.slice(action.index + 1)
        ]
      })
    case DECREMENT_COUNTER:
      console.log('decrementing the counter' + action.index);
      return Object.assign({}, {
        counters: [
          ...state.counters.slice(0, action.index),
          state.counters[action.index] - 1,
          ...state.counters.slice(action.index + 1)
        ]
      })
    case ADD_COUNTER:
      return Object.assign({}, {
        counters: [
          ...state.counters, 0
        ]
      })
    case REMOVE_COUNTER:
      return Object.assign({}, {
        counters: [
          ...state.counters.slice(0, -1)
        ]
      })
    default:
      return state;
  }
}

export default counters;