import React, { PropTypes, Component } from 'react';
import Counter from './Counter';

function CounterGrid({ counters, onIncrement, onDecrement, addCounter, removeCounter }) {
  console.log(counters);
  return (
    <div>
      <button type="button" onClick={addCounter}>Add Counter</button>
      <button type="button" onClick={removeCounter}>Remove Counter</button>
      {counters.map((count, index) => 
        <Counter
          key={index}
          count={count}
          onIncrement={() => onIncrement(index)}
          onDecrement={() => onDecrement(index)}
        />
      )}
    </div>
  )
}

export default CounterGrid;