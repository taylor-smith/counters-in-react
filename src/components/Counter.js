import React, { Component } from 'react';

class Counter extends Component {
  render() {
    const { count, onIncrement, onDecrement } = this.props;
    return (
      <div style={{display:'inline-block'}}>
        <h1 className="counter">{count}</h1>
        <button className="onDecrement" onClick={onDecrement}>-</button>
        <button className="onIncrement" onClick={onIncrement}>+</button>
      </div>
    )
  }
}

export default Counter;